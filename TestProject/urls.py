from django.conf.urls import patterns, include, url
from django.contrib import admin


from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'TestProject.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),


    url(r'^myapp/', include('myapp.urls')),
    url(r'^admin/', include(admin.site.urls)),
)


if True:
    urlpatterns += patterns(
        'django.views.static',
        (r'^media/(?P<path>.*)',
        'serve',
        {'document_root': settings.MEDIA_ROOT}), )

 