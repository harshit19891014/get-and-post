from django.forms import ModelForm
from django.contrib.auth.models import User
from  .models import *


class UserForm(ModelForm):
    #password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'email', 'password')


class ProfileForm(ModelForm):
	class Meta:
		model=Profile
		fields=['mobile','image',]